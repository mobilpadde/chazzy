package com.mwc.chazzy.ui.chat

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.mwc.chazzy.data.message.Message
import com.mwc.chazzy.data.message.MessageRepository
import com.mwc.chazzy.data.message.MessagesRoomDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ChatViewModel(application: Application): AndroidViewModel(application) {
    private val repo: MessageRepository
    val latestMessages: LiveData<List<Message>>

    init {
        val messageDao = MessagesRoomDatabase.getDatabase(application).messageDao()
        repo = MessageRepository(messageDao)
        latestMessages = repo.latestMessages
    }

    fun insert(msg: Message) = viewModelScope.launch(Dispatchers.IO) {
        repo.insert(msg)
    }
}