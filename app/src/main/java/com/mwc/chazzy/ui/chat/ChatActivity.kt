package com.mwc.chazzy.ui.chat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mwc.chazzy.R

class ChatActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
    }
}
