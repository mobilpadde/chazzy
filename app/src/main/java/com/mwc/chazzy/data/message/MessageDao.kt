package com.mwc.chazzy.data.message

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface MessageDao {
    @Query("SELECT * FROM messages WHERE roomId = :roomId ORDER BY timestamp DESC LIMIT 50")
    fun getLatestMessages(roomId: String): LiveData<List<Message>>

    @Insert
    suspend fun insert(msg: Message)

    @Query("DELETE FROM messages")
    fun deleteAll()
}