package com.mwc.chazzy.data.message

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Message::class], version = 1)
abstract class MessagesRoomDatabase: RoomDatabase() {
    abstract fun messageDao(): MessageDao

    companion object {
        @Volatile
        private var INSTANCE: MessagesRoomDatabase? = null

        fun getDatabase(context: Context): MessagesRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MessagesRoomDatabase::class.java,
                    "messages_db"
                ).build()

                INSTANCE = instance
                instance
            }
        }
    }
}