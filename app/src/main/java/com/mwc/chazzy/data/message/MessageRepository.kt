package com.mwc.chazzy.data.message

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class MessageRepository(private val messageDao: MessageDao) {
    val latestMessages: LiveData<List<Message>>  = messageDao.getLatestMessages()

    @WorkerThread
    suspend fun insert(msg: Message) {
        messageDao.insert(msg)
    }
}