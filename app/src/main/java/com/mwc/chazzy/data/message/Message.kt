package com.mwc.chazzy.data.message

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.sql.Timestamp

@Entity(tableName = "messages")
data class Message(
    @PrimaryKey @ColumnInfo(name = "userId") val userId: String,
    @ColumnInfo(name = "roomId") val roomId: String,
    @ColumnInfo(name = "message") val msg: String,
    @ColumnInfo(name = "timestamp") private val ts: Timestamp = Timestamp(System.currentTimeMillis())
)